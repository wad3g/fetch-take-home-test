// prefer using Styled Components but overkill in this example
export const styles = {
  listGroupDivder: {
    margin: "0.25ch",
    borderColor: "#dadada",
    borderLeftColor: "white",
    borderStyle: "solid",
    borderWidth: "0.0125ch",
    borderLeftWidth: "20ch",
  },
  listGroupLabels: {
    fontWeight: 600,
    fontSize: "18px",
    fontFamily: "monospace",
    position: "absolute",
    letterSpacing: "-1px",
    lineHeight: 1.15,
    margin: 0,
  },
  listGroupItems: {
    fontWeight: 400,
    fontSize: 18,
    fontFamily: "monospace",
    marginLeft: "8ch",
  },
};
