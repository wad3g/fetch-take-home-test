import { useEffect, useState } from "react";
import { BASE_URL } from "../utils/constants";

const useFetch = (url) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);

  useEffect(
    () => {
      const getData = () =>
        fetch(BASE_URL + url, {
          headers: {
            Accept: "application/json",
          },
        })
          .then((response) => {
            return response.json();
          })
          .then((results) => {
            setData(results);
            setIsLoading(false);
          })
          .catch(setHasError(true), setIsLoading(false), (error) => {
            console.log(error);
          });

      getData();
    },
    [url],
    {}
  );
  return { data, isLoading, hasError };
};

export default useFetch;
