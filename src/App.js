import React from "react";
import { useFetch } from "./hooks";
import { styles } from "./utils/styles";

const App = () => {
  const { data, isLoading, hasError } = useFetch("/hiring.json");

  let results = data.length > 1 ? data : [];
  const listGroups = results
    // filter falsy values in results
    .filter((res) => res?.name) // filter falsy results
    // group results
    .reduce((res, val) => {
      if (!res[val.listId]) {
        res[val.listId] = [];
      }
      // groupBy listId
      res[val.listId].push(val);

      return res;
    }, {});

  for (var key in listGroups)
    listGroups[key].sort(function (x, y) {
      // split name to sort by id
      const [, nameIdX] = x.name.split(" ");
      const [, nameIdY] = y.name.split(" ");

      return parseInt(nameIdX) - parseInt(nameIdY);
    });

  const listGroupLabels = Object.keys(listGroups);

  const displayData = () =>
    listGroupLabels.map((listGroup, i) => (
      <div key={i.toString()}>
        {/* don"t show divider for first listGroup */}
        {listGroups[i] + 1 ? <hr style={styles.listGroupDivder} /> : ""}
        <h4 style={styles.listGroupLabels}>{"listId " + listGroupLabels[i]}</h4>

        {listGroups[listGroup].map((listGroupItem, idx) => (
          <div style={styles.listGroupItems} key={idx.toString()}>
            {listGroupItem.name}
          </div>
        ))}
      </div>
    ));

  const displayMsg = () =>
    isLoading ? (
      <p>Loading Data</p>
    ) : results.length !== 0 || !hasError ? (
      displayData()
    ) : (
      <p>There was an issue fetching your data.</p>
    );

  return <div>{displayMsg()}</div>;
};

export default App;
